# ja_gitlab_grpc_proto

- improve the grpc-proto updated method
- separate the grpc to independent repo.
- other services, simply add git-submodule by clone the 'ja_gitlab_grpc_proto' into folder 'proto' 


---

## Version control with Go moudle
---
- See Structure 😈
---

![img](img/go_module_version_control.jpg)

---

- master rooot foler - v1.0.0  with tag v1.0.1, tag v1.1.2 etc..
- make folder v2 , with tag v2.0.0, tag v2.0.1 etc..
- try v1.0.0 initial first 🐸 
- run the gitlab_tag_cmd_history (push origin v1.0.0 ... 🦜..)
 now yuu can see the tag is in the repository ✅
---

![result](img/gitlab_v100.jpg)
---

go get github.com/jwojfowjf/fjwojfwoi@v1.0.0
Voilla ✅ 
---

![result](img/go_get_version.jpg)

---


## Proto
---
user_proto : User-Server used (v1)


## For other repos to add this into sub-module
---

move ja_renew.sh to server_repo comme ca

- server_repo
    - ja_renew_proto.sh

by running source ja_renew_proto.sh 


---
then...

- server_repo
    - ja_renew_proto.sh
    - proto (appeaer now)


## For golang to create user_proto/user_proto.pb.go
---

- go_mod_server_root
    - ja_grpc_create_user_proto.sh
    - ja_create_golang_env.sh 
    - proto
    - nothing
---
After run this 🛢

source ja_create_golang_env.sh

---

Become this 😈

- go_mod_server_root
    - ja_grpc_create_user_proto.sh
    - ja_create_golang_env.sh 
    - proto
    - user_proto ✅

